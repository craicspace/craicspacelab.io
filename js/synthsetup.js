var Synth = window.Synth;
var _ = window._;

// var NOTES = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'];
var NOTES = ['A#', 'C#', 'D#', 'F#', 'G#'];

/**
 * Plays synth note.
 *
 * @param {String|Number} instrument
 * @param {String}        note
 * @param {Number}        octave
 * @param {Number}        duration
 */
function playSynth(instrument, note, octave, duration) {
  console.log(arguments);
  Synth.play.apply(Synth, arguments);
}

// cache elements
var octave1 = 2;
var octave2 = 4;
var duration = 3;

/**
 * Plays random note.
 */
function playRandomNote() {
  playSynth(
    'edm',
    _.sample(NOTES),
    _.random(
      parseInt(octave1),
      parseInt(octave2)
    ),
    parseInt(duration)
  );
}

function makemusic() {
	playRandomNote();
	setTimeout(makemusic, 500);
}

